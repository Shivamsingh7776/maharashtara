import csv
import matplotlib.pyplot as plt

no_of_com = {}

with open('Maharashtra.csv',mode='r',encoding='ISO-8859-1') as M:
    data = csv.DictReader(M)
#2. Bar Plot of company registration by year
    for row in data:
        date1 = row['DATE_OF_REGISTRATION']
        y= date1[-2:]
        
        if no_of_com.get(y) == None:
            no_of_com[y]=1
        else:
            no_of_com[y]+=1



plt.bar(list(no_of_com.keys()),list(no_of_com.values()))
plt.xticks(rotation = 90)
plt.tight_layout()
plt.show()




        