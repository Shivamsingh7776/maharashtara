import csv
import matplotlib.pyplot as plt

no_of_com = {}
cnt1,cnt2,cnt3,cnt4,cnt5 = 0,0,0,0,0
with open('Maharashtra.csv',mode='r',encoding='ISO-8859-1') as M:
    data = csv.DictReader(M)

    for row in data:
        Auth_data = row['AUTHORIZED_CAP']
        if (Auth_data) <= '100000':
            cnt1+=1
        elif (Auth_data) > '100000' and (Auth_data) <= '1000000':
            cnt2 += 1
        elif (Auth_data) > '1000000' and (Auth_data) <= '10000000':
            cnt3 += 1
        elif (Auth_data) > '10000000' and (Auth_data) <= '100000000':
            cnt4 += 1
        elif (Auth_data) > '100000000':
            cnt5 += 1

x_level = ['<=1L','1L to 10L','10L to 1Cr','1Cr to 10Cr','> 10Cr']
y_level = [cnt1,cnt2,cnt3,cnt4,cnt5]

plt.bar(x_level,y_level,color = 'r')
plt.show()





